package tr.edu.mu.ceng.gui.circle;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;


public class Chr extends AppCompatActivity {
    private Chronometer chronometer;
    private boolean isStart;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chr);

        chronometer = findViewById(R.id.chronometer);

        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometerChanged) {
                chronometer = chronometerChanged;
            }
        });
    }
    public void startStopChronometer(View view){
        if(isStart){
            chronometer.stop();
            isStart = false;
            ((Button)view).setText(getString(R.string.ss));
        }else{
            chronometer.setBase(SystemClock.elapsedRealtime());
            chronometer.start();
            isStart = true;
            ((Button)view).setText(getString(R.string.s));
        }
    }
}