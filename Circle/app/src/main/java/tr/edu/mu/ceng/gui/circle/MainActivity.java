package tr.edu.mu.ceng.gui.circle;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.hitomi.cmlibrary.CircleMenu;
import com.hitomi.cmlibrary.OnMenuSelectedListener;



public class MainActivity extends AppCompatActivity {

    String arrayName[] = {"Alarm",
            "Graph",
            "Timer",
            "Settings",
            "Chronometer"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CircleMenu circleMenu = findViewById(R.id.circle_menu);
        circleMenu.setMainMenu(Color.parseColor("#CDCDCD"), R.drawable.menu, R.drawable.cross)
                .addSubMenu(Color.parseColor("#258CFF"), R.drawable.clock)
                .addSubMenu(Color.parseColor("#FF0000"), R.drawable.graph)
                .addSubMenu(Color.parseColor("#6d4c41"), R.drawable.timer)
                .addSubMenu(Color.parseColor("#FF0000"), R.drawable.set)
                .addSubMenu(Color.parseColor("#FF0000"), R.drawable.chr)
                .setOnMenuSelectedListener(new OnMenuSelectedListener() {
                    @Override
                    public void onMenuSelected(int index) {
                        switch (index) {
                            case 0:
                                Toast.makeText(MainActivity.this, arrayName[0], Toast.LENGTH_SHORT).show();
                                Intent alarm = new Intent(MainActivity.this, Alarm.class);
                                startActivity(alarm);
                                break;
                            case 1:
                                Toast.makeText(MainActivity.this, "Graph", Toast.LENGTH_SHORT).show();
                                Intent graph = new Intent(MainActivity.this, tr.edu.mu.ceng.gui.circle.Graph.class);
                                startActivity(graph);
                                break;
                            case 2:
                                Toast.makeText(MainActivity.this, "Timer", Toast.LENGTH_SHORT).show();
                                Intent timer = new Intent(MainActivity.this, Timer.class);
                                startActivity(timer);
                                break;
                            case 3:
                                Toast.makeText(MainActivity.this, "Settings", Toast.LENGTH_SHORT).show();
                                Intent settings = new Intent(MainActivity.this, Settings.class);
                                startActivity(settings);
                                break;
                            case 4:
                                Toast.makeText(MainActivity.this, "Chronometer", Toast.LENGTH_SHORT).show();
                                Intent chronometer = new Intent(MainActivity.this, Chr.class);
                                startActivity(chronometer);
                                break;
                        }
                    }

                });

    }}
